<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package   local_userscontrol
 * @copyright 2020, You Name <isaactmp@gmail.com>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// External Files
require_once (__DIR__.'/../../config.php');
require_once($CFG->dirroot.'/local/userscontrol/lib.php');

// Page parameters
$PAGE->set_context(\context_system::instance());
$PAGE->set_title('Tabla de usuarios');

// Check Permission
if(!has_capability('local/userscontrol:userscontrol', $PAGE->context)) {
    $url = new moodle_url('/');
    redirect($url);
    die();
}

// Pagination
$page = optional_param('page', 0, PARAM_INT); // Which page to show.
$perpage = 2; // How many per page.
$PAGE->set_url( '/local/userscontrol/manage.php', array(
    'page' => $page,
    'perpage' => $perpage
));

// Header
echo $OUTPUT->header();

// Users
$users = array_values(userscontrol_get_users());
$total = count($users);
$users = array_slice($users, $page, $perpage, true);

// Table
$table = new html_table();
$table->head = array ();
$table->colclasses = array();
$table->attributes['class'] = 'admintable generaltable table-sm';
$table->id = "users";

// Define head columns
$table->head[] = 'username';
$table->head[] = 'Nombre';
$table->head[] = 'Apellido';
$table->head[] = 'Curso';

// Foreach Users
foreach ($users as $user) {
    $row = array ();
    $row[] = $user->username;
    $row[] = $user->firstname;
    $row[] = $user->lastname;
    $lastcolumn = ''; // Last column string
    $courses = enrol_get_users_courses($user->id); // Get user enrolments
    foreach($courses as $course){
        $lastcolumn.=$course->fullname.'<br>';
    }
    $row[] = $lastcolumn;
    $table->data[] = $row;
}

// Print table
if (!empty($table)) {
    echo $OUTPUT->paging_bar($total, $page, $perpage, $PAGE->url); // Pagination
    echo $OUTPUT->download_dataformat_selector(get_string('userscontrol', 'local_userscontrol'), 'download.php'); // Donwload file selector
    echo html_writer::start_tag('div', array('class' => 'no-overflow'));
    echo html_writer::table($table);
    echo html_writer::end_tag('div');
    echo $OUTPUT->paging_bar($total, $page, $perpage, $PAGE->url); // Pagination
}

// Footer
echo $OUTPUT->footer();