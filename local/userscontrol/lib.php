<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package   local_userscontrol
 * @copyright 2020, You Name <isaactmp@gmail.com>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

function local_userscontrol_extend_navigation(global_navigation $navigation) {
    if(!has_capability('moodle/site:config',context_system::instance())){
        return;
    }

    $main_node = $navigation->add(get_string('userscontrol','local_userscontrol'),'/local/userscontrol/manage.php');
    $main_node->nodetype = 1;
    $main_node->collapse = false;
    $main_node->forceopen = true;
    $main_node->isexpandable = false;
    $main_node->showinflatnavigation = true;
    $main_node->icon = new pix_icon('i/users', 'users');
}

function userscontrol_get_users(){
    global $DB;

    $sql = 'SELECT u.id, u.username, u.firstname, u.lastname, e.userid FROM {user} u '; // Base
    $sql.= 'LEFT JOIN {role_assignments} e ON u.id = e.userid '; // LEFT Join user_enrolments
    $sql.= 'WHERE e.roleid = 5 GROUP BY u.firstname ORDER BY u.username';

    $users = $DB->get_records_sql($sql);

    return $users;
}