<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * ${PLUGINNAME} file description here.
 *
 * @package    local_userscontrol
 * @copyright  2021 SysBind Ltd. <service@sysbind.co.il>
 * @auther     isaac.petruccelli
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// External Files
require_once(__DIR__.'/../../config.php');
$PAGE->set_context(\context_system::instance());
$PAGE->set_title('Tabla de usuarios');


// Check Permission
if(!has_capability('local/userscontrol:userscontrol', $PAGE->context)) {
    $url = new moodle_url('/');
    redirect($url);
    die();
}

// Aditional Files
require_once($CFG->dirroot.'/local/userscontrol/lib.php');

// Parameters
$dataformat = optional_param("dataformat",'csv',PARAM_ALPHA); // Which page to show.
$PAGE->set_url( '/local/userscontrol/download.php', array(
    'dataformat' => $dataformat,
));


// Users
$users = userscontrol_get_users();

// Define header columns
$columns = array(
    'username' => 'username',
    'firstname' => 'Nombre',
    'lastname' => 'Apellido',
    'courses' => 'Cursos',
);

$filename = 'report_users'; // Filename
\core\dataformat::download_data($filename, $dataformat, $columns, $users, function($user) {
    // Process the data in some way.
    // You can add and remove columns as needed
    // as long as the resulting data matches the $column metadata.
    $enrolments = '';
    $courses = enrol_get_users_courses($user->id);
    $indexc = 0;
    foreach($courses as $course){
        if(count($courses) > 1){
            $enrolments.=$course->fullname;
            $enrolments.=((count($courses) - 1) == $indexc) ? '' : ', ';
        }else{
            $enrolments.=$course->fullname;
        }
        $indexc++;
    }
    return array(
        'username' => $user->username,
        'firstname' => $user->firstname,
        'lastname' => $user->lastname,
        'courses' => $enrolments,
    );
});
